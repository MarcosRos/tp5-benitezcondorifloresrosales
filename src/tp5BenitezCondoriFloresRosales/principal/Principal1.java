package tp5BenitezCondoriFloresRosales.principal;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import tp5BenitezCondoriFloresRosales.tablas.TablaDesarrolladores;
import tp5BenitezCondoriFloresRosales.tablas.TablaJefesProyectos;
import tp5BenitezCondoriFloresRosales.JefeProyecto;
import tp5BenitezCondoriFloresRosales.Pasante;
import tp5BenitezCondoriFloresRosales.PersonalDeDesarrollo;
import tp5BenitezCondoriFloresRosales.Presidente;
import tp5BenitezCondoriFloresRosales.Proyecto;
import tp5BenitezCondoriFloresRosales.ResponsableDeRecursos;
import tp5BenitezCondoriFloresRosales.Tarea;
import tp5BenitezCondoriFloresRosales.tablas.TablaPasantes;
import tp5BenitezCondoriFloresRosales.tablas.TablaPresidentes;
import tp5BenitezCondoriFloresRosales.tablas.TablaProyectos;
import tp5BenitezCondoriFloresRosales.tablas.TablaRRHH;
import tp5BenitezCondoriFloresRosales.tablas.TablaTareas;

public class Principal1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		String respuesta;
		do {
			System.out.println("Desea iniciar sesion, registrarse o cargar usuarios predefinidos?");
			System.out.println("Ingrese i, r o p segun corresponda");
			System.out.println("Ingrese Off si desea salir");
			respuesta = scan.next();
			String rol, username, password, nombre, tipo, tipo2, passwordaux, aux, tecnologia, tituloPro;
			int dia, mes, anio, dni, legajo, nroContacto;
			LocalDate fNac, fIngreso, fechaInP, fechaFinP;
			Boolean estado = false, first=true;
			
			
			switch (respuesta) {
			case "r": {
				first=false;
				System.out.println("Ingrese el dni del nuevo usuario");
				dni = scan.nextInt();
				System.out.println("Ingrese el nombre del nuevo usuario");
				nombre = scan.next();

				System.out.println("Ingrese dia de nacimiento");
				dia = scan.nextInt();
				System.out.println("Ingrese mes de nacimiento");
				mes = scan.nextInt();
				System.out.println("Ingrese anio de nacimiento");
				anio = scan.nextInt();
				fNac = LocalDate.of(anio, mes, dia);

				do {
					System.out.println("Ingrese el nombre con el que iniciara sesion");
					username = scan.next();
				} while (buscarUsuario(username) == true);

				do {
					System.out.println("Ingrese la contraseña con la que iniciara sesion");
					password = scan.next();
					System.out.println("Confirme la contraseña");
					passwordaux = scan.next();
				} while (!password.equals(passwordaux));

				do {
					System.out.println("Seleccione el tipo de usuario");
					System.out.println("Ingrese P para seleccionar -> Pasante");
					System.out.println("Ingrese C para seleccionar -> Personal Contratado");
					tipo = scan.next();
					if (!tipo.equals("P") && !tipo.equals("C"))
						System.out.println("Opcion inexistente, controle mayusculas");
				} while (!tipo.equals("P") && !tipo.equals("C"));

				if (tipo.equals("C")) {
					System.out.println("Ingrese dia de ingreso a la empresa");
					dia = scan.nextInt();
					System.out.println("Ingrese mes de ingreso a la empresa");
					mes = scan.nextInt();
					System.out.println("Ingrese anio de ingreso a la empresa");
					anio = scan.nextInt();
					fIngreso = LocalDate.of(anio, mes, dia);
					System.out.println("Seleccione el legajo del usuario");
					legajo = scan.nextInt();

					do {
						System.out.println("Seleccione el tipo de usuario");
						System.out.println("Ingrese P para seleccionar -> Presidente");
						System.out.println("Ingrese RR para seleccionar -> Responsable de Recursos Humanos");
						System.out.println("Ingrese PD para seleccionar -> Personal de Desarrollo");
						tipo2 = scan.next();
						if (!tipo2.equals("P") && !tipo2.equals("RR") && !tipo2.equals("PD"))
							System.out.println("Opcion inexistente, controle mayusculas");
					} while (!tipo2.equals("P") && !tipo2.equals("RR") && !tipo2.equals("PD"));

					if (tipo2.equals("PD")) {
						do {
							System.out.println("Seleccione el rol de Personal de Desarrollo");
							System.out.println("Ingrese P para seleccionar -> Programador");
							System.out.println("Ingrese A para seleccionar -> Analista");
							System.out.println("Ingrese DG para seleccionar -> Dise�ador grafico");
							System.out.println("Ingrese AS para seleccionar -> Arquitecto de software");
							System.out.println("Ingrese JP para seleccionar -> Jefe de proyecto.");
							aux = scan.next();
							switch (aux) {
							case "P":
								rol = "Programador";
								break;
							case "A":
								rol = "Analista";
								break;
							case "DG":
								rol = "Dise�ador Grafico";
								break;
							case "AS":
								rol = "Arquitecto de Software";
								break;
							case "JP":
								rol = "Jefe de Proyecto";
								break;
							default:
								System.out.println("Opcion inexistente, controle mayusculas");
								rol = "null";
								break;
							}
						} while (!aux.equals("P") && !aux.equals("A") && !aux.equals("DG") && !aux.equals("AS")
								&& !aux.equals("JP"));

						System.out.println("Ingrese su numero de contacto");
						nroContacto = scan.nextInt();
						System.out.println("Ingrese su titulo Profesional");
						tituloPro = scan.next();
						System.out.println("Ingrese las tecnologias que utiliza");
						tecnologia = scan.next();

						if (rol.equals("Jefe de Proyecto")) {
							JefeProyecto unJefeProyecto = new JefeProyecto(dni, nombre, fNac, rol, username, password,
									fIngreso, legajo, nroContacto, tituloPro, tecnologia, estado);
							TablaJefesProyectos.jefesProyectos.add(unJefeProyecto);
						} else {
							PersonalDeDesarrollo unDesarrollador = new PersonalDeDesarrollo(dni, nombre, fNac, rol,
									username, password, fIngreso, legajo, nroContacto, tituloPro, tecnologia, estado);
							TablaDesarrolladores.desarrolladores.add(unDesarrollador);
						}
					} else {
						rol = "Gerente";
						if (tipo2.equals("RR")) {
							ResponsableDeRecursos unResponsableDeRecursos = new ResponsableDeRecursos(dni, nombre, fNac,
									rol, username, password, fIngreso, legajo);
							TablaRRHH.RRHHs.add(unResponsableDeRecursos);

						} else if (tipo2.equals("P")) {
							Presidente unPresidente = new Presidente(dni, nombre, fNac, rol, username, password,
									fIngreso, legajo);
							TablaPresidentes.presidentes.add(unPresidente);
						}
					}

				} else {
					do {
						System.out.println("Seleccione el rol de Pasante");
						System.out.println("Ingrese P para seleccionar -> Programador");
						System.out.println("Ingrese A para seleccionar -> Analista");
						System.out.println("Ingrese DG para seleccionar -> Dise�ador grafico");
						System.out.println("Ingrese AS para seleccionar -> Arquitecto de software");
						aux = scan.next();
						switch (aux) {
						case "P":
							rol = "Programador";
							break;
						case "A":
							rol = "Analista";
							break;
						case "DG":
							rol = "Dise�ador Grafico";
							break;
						case "AS":
							rol = "Arquitecto de Software";
							break;
						default:
							System.out.println("Opcion inexistente, controle mayusculas");
							rol = "null";
							break;
						}
					} while (!aux.equals("P") && !aux.equals("A") && !aux.equals("DG") && !aux.equals("AS"));
					System.out.println("Ingrese la universidad a la que pertenece");
					String universidad = scan.next();
					System.out.println("Ingrese dia de inicio de Pasantia");
					dia = scan.nextInt();
					System.out.println("Ingrese mes de inicio de Pasantia");
					mes = scan.nextInt();
					System.out.println("Ingrese anio de inicio de Pasantia");
					anio = scan.nextInt();
					fechaInP = LocalDate.of(anio, mes, dia);
					System.out.println("Ingrese dia de fin de Pasantia");
					dia = scan.nextInt();
					System.out.println("Ingrese mes de fin de Pasantia");
					mes = scan.nextInt();
					System.out.println("Ingrese anio de fin de Pasantia");
					anio = scan.nextInt();
					fechaFinP = LocalDate.of(anio, mes, dia);
					;
					System.out.println("Ingrese la tecnologia que utiliza");
					tecnologia = scan.next();

					Pasante unPasante = new Pasante(dni, nombre, fNac, rol, username, password, universidad, fechaInP,
							fechaFinP, tecnologia, estado);
					TablaPasantes.pasantes.add(unPasante);
				}
			}

				break;
			case "i": {
				Boolean bandaux = false;
				do {
					System.out.println("Ingrese su usuario");
					username = scan.next();
					System.out.println("Ingrese su contrasenia");
					password = scan.next();
					bandaux = buscarUsuarioInicio(username, password);
					if (bandaux == false) {
						System.out.println("Usuario o contrase�a incorrectos!");
					}
				} while (bandaux == false);

				String tipoDeUsuario = tipoUsuario(username);
				switch (tipoDeUsuario) {
				case "Desarrollador":
					PersonalDeDesarrollo unDesarrollador = buscarDesarrollador(username);
					int opcionDesarrollador;
					do {
						System.out.println("--------Menu Desarrollador--------");
						System.out.println("Escoja una opcion");
						System.out.println("1) Listar Tareas");
						System.out.println("2) Buscar Proyecto por Codigo");
						System.out.println("3) Buscar Tarea por Codigo");
						System.out.println("4) Modificar Tarea por Codigo");
						System.out.println("5) Salir");
						opcionDesarrollador = scan.nextInt();

						switch (opcionDesarrollador) {
						case 1:
							System.out.println("Mostrando todas las tareas: ");
							unDesarrollador.listarTareas();

							break;
						case 2:
							int codProyecto;
							if (TablaProyectos.proyectos.isEmpty() == true) {
								System.out.println("La tabla proyectos se encuentra vacia");
							} else {
								System.out.println("Ingrese el codigo del Proyecto a buscar");
								codProyecto = scan.nextInt();
								unDesarrollador.buscarProyectoCod(codProyecto);
							}

							break;
						case 3:
							int codTarea;

							if (TablaTareas.tareas.isEmpty() == true) {
								System.out.println("La tabla tareas se encuentra vacia");
							} else {
								System.out.println("Ingrese el codigo de la Tarea a buscar");
								codTarea = scan.nextInt();
								unDesarrollador.buscarTareaCod(codTarea);
							}

							break;
						case 4:
							int codTareaModificar, opcionTareaModificar;
							String obsTareaModificar, estadoString;
							boolean estadoTareaModificar,bandEstado=false;

							if (TablaTareas.tareas.isEmpty() == true) {
								System.out.println("La tabla tareas se encuentra vacia");
							}

							else {

								System.out.println("1)Modificar Observacion");
								System.out.println("2)Modificar Estado");
								opcionTareaModificar = scan.nextInt();

								if (opcionTareaModificar == 1) {
									System.out.println("Ingrese el codigo de la tarea a modificar");
									codTareaModificar = scan.nextInt();
									System.out.println("Ingrese la observacion de la tarea");
									obsTareaModificar = scan.next();
									unDesarrollador.modificarTareaObs(codTareaModificar, obsTareaModificar);
								} else if (opcionTareaModificar == 2) {
									System.out.println("Ingrese el codigo de la tarea a modificar");
									codTareaModificar = scan.nextInt();
									do {
									System.out.println("Ingrese el estado de la tarea (Activo o Inactivo)");
									estadoString=scan.next();
									switch (estadoString) {
									case "Activo":
										estadoTareaModificar = true;
										unDesarrollador.modificarTareaEstado(codTareaModificar, estadoTareaModificar);
										bandEstado=true;
										break;
									case "Inactivo":
										estadoTareaModificar = false;
										unDesarrollador.modificarTareaEstado(codTareaModificar, estadoTareaModificar);
										bandEstado=true;
										break;
										default:
											System.out.println("Estado no aceptable, respete mayusculas y minusculas");
										break;
									}
									}while(bandEstado==false);
								} else {
									System.out.println("Opcion Invalida");
								}
							}
							break;
						case 5:
							System.out.println("Saliendo del Programa");
							break;
						default:
							System.out.println("Opcion invalida");
							break;
						}
					} while (opcionDesarrollador != 5);
					break;
				case "Presidente":
					Presidente unPresidente = buscarPresidente(username);

					int opcionPresidente;
					do {
						System.out.println("----------Menu Presidente---------");
						System.out.println("Escoja una opcion");
						System.out.println("1) Listar Proyectos por porcentaje");
						System.out.println("2) Listar proyectos por estado");
						System.out.println("3) Salir");
						opcionPresidente = scan.nextInt();

						switch (opcionPresidente) {
						case 1:
							if (TablaProyectos.proyectos.isEmpty() == true) {
								System.out.println("La tabla proyectos se encuentra vacia");
							} else {
								unPresidente.listarAvance();
							}
							break;
						case 2:
							if (TablaProyectos.proyectos.isEmpty() == true) {
								System.out.println("La tabla proyectos se encuentra vacia");
							} else {
								unPresidente.listarEstado();
							}
							break;
						case 3:
							System.out.println("Saliendo del Programa");
							break;
						default:
							System.out.println("Opcion invalida");
							break;
						}
					} while (opcionPresidente != 3);
					break;
				case "Pasante":
					Pasante unPasante = buscarPasante(username);
					int opcionPasante;
					do {
						System.out.println("----------Menu Pasante---------");
						System.out.println("Escoja una opcion");
						System.out.println("1) Listar Tareas");
						System.out.println("2) Buscar Proyecto por Codigo");
						System.out.println("3) Buscar Tarea por Codigo");
						System.out.println("4) Modificar Tarea por Codigo");
						System.out.println("5) Salir");
						opcionPasante = scan.nextInt();

						switch (opcionPasante) {
						case 1:
							System.out.println("Mostrando todas las tareas: ");
							unPasante.listarTareas();

							break;
						case 2:
							int codProyecto;

							if (TablaProyectos.proyectos.isEmpty() == true) {
								System.out.println("La tabla proyectos se encuentra vacia");
							} else {
								System.out.println("Ingrese el codigo del Proyecto a buscar");
								codProyecto = scan.nextInt();
								unPasante.buscarProyectoCod(codProyecto);
							}
							break;
						case 3:
							int codTarea;

							if (TablaTareas.tareas.isEmpty() == true) {
								System.out.println("La tabla tareas se encuentra vacia");
							} else {
								System.out.println("Ingrese el codigo de la Tarea a buscar");
								codTarea = scan.nextInt();
								unPasante.buscarTareaCod(codTarea);
							}

							break;
						case 4:
							int codTareaModificar, opcionTareaModificar;
							String obsTareaModificar, estadoString;
							boolean estadoTareaModificar,bandEstado=false;

							if (TablaTareas.tareas.isEmpty() == true) {
								System.out.println("La tabla tareas se encuentra vacia");
							}

							else {

								System.out.println("1)Modificar Observacion");
								System.out.println("2)Modificar Estado");
								opcionTareaModificar = scan.nextInt();

								if (opcionTareaModificar == 1) {
									System.out.println("Ingrese el codigo de la tarea a modificar");
									codTareaModificar = scan.nextInt();
									System.out.println("Ingrese la observacion de la tarea");
									obsTareaModificar = scan.next();
									unPasante.modificarTareaObs(codTareaModificar, obsTareaModificar);
								} else if (opcionTareaModificar == 2) {
									System.out.println("Ingrese el codigo de la tarea a modificar");
									codTareaModificar = scan.nextInt();
									do {
									System.out.println("Ingrese el estado de la tarea (Activo o Inactivo)");
									estadoString=scan.next();
									switch (estadoString) {
									case "Activo":
										estadoTareaModificar = true;
										unPasante.modificarTareaEstado(codTareaModificar, estadoTareaModificar);
										bandEstado=true;
										break;
									case "Inactivo":
										estadoTareaModificar = false;
										unPasante.modificarTareaEstado(codTareaModificar, estadoTareaModificar);
										bandEstado=true;
										break;
										default:
											System.out.println("Estado no aceptable, respete mayusculas y minusculas");
										break;
									}
									}while(bandEstado==false);
								} else {
									System.out.println("Opcion Invalida");
								}
							}
							break;
						case 5:
							System.out.println("Saliendo del Programa");
							break;
						default:
							System.out.println("Opcion invalida");
							break;
						}
					} while (opcionPasante != 5);
					break;
				case "JefeP":
					JefeProyecto unJefeProyecto = buscarJefeProyecto(username);
					int opcionJefe;
					do {
						System.out.println("----------Menu Jefe de Proyecto---------");
						System.out.println("Escoja una opcion");
						System.out.println("1) Crear Proyecto");
						System.out.println("2) Crear Tarea");
						System.out.println("3) Agregar Usuario a Tarea");
						System.out.println("4) Listar Tareas");
						System.out.println("5) Buscar Proyecto por Codigo");
						System.out.println("6) Buscar Tarea por Codigo");
						System.out.println("7) Modificar Tarea por Codigo");
						System.out.println("8) Salir");
						opcionJefe = scan.nextInt();

						switch (opcionJefe) {
						case 1:
							
						    System.out.println("Selecciono Crear Proyecto");
							
						    int porcentajeProyectoCreado, codigoProyectoCreado, contadorProyectoCreado, diaProyectoCreado, mesProyectoCreado, anioProyectoCreado;
						    String nombreProyectoCreado, jefeProyectoCreado,estadoProyectoCreado;
						    boolean bandProyectoCreado, bandEstado=false;
						    LocalDate fechaEntregaProyectoCreado;
						    
						    bandProyectoCreado=false;
							contadorProyectoCreado = 0;
							do {
								System.out.println("Ingrese el Codigo");
								codigoProyectoCreado = scan.nextInt();
								if(TablaProyectos.proyectos.isEmpty()==true) {
									System.out.println("Codigo Disponible");
									bandProyectoCreado = true;
								}
								else {
									while(contadorProyectoCreado != TablaProyectos.proyectos.size()) {
										
										if (codigoProyectoCreado == TablaProyectos.proyectos.get(contadorProyectoCreado).getCodigo()) {
											bandProyectoCreado=true;
										}
										else{
											contadorProyectoCreado++;
										}
									}
									if(bandProyectoCreado==true) {
										System.out.println("El codigo ingresado ya esta siendo usado por otro Proyecto");
										bandProyectoCreado = false;
									}
									else{
										System.out.println("Codigo Disponible");
										bandProyectoCreado = true;
									}
								}
								
							}while (bandProyectoCreado == false);
							
							System.out.println("Ingrese el Nombre");
							nombreProyectoCreado = scan.next();
							System.out.println("El jefe de Proyecto del nuevo Proyecto Creado ha sido asignado con su usuario");
							jefeProyectoCreado = unJefeProyecto.getUsername();
							Boolean valido=false;
							do {
								System.out.println("Ingrese el estado del proyecto (Activo o Inactivo)");
								estadoProyectoCreado=scan.next();
								
								switch (estadoProyectoCreado) {
								case "Activo":
									bandEstado = true;
									valido=true;
									break;
								case "Inactivo":
									bandEstado = false;
									valido=true;
									break;
									default:
										System.out.println("Estado no aceptable, respete mayusculas y minusculas");
									break;
								}
								}while(valido==false);
							
							System.out.println("Ingrese el Porcentaje");
							porcentajeProyectoCreado = scan.nextInt();
							System.out.println("Ingrese el dia de entrega");
							diaProyectoCreado = scan.nextInt();
							System.out.println("Ingrese el mes de entrega");
							mesProyectoCreado = scan.nextInt();
							System.out.println("Ingrese el anio de entrega");
							anioProyectoCreado = scan.nextInt();
							fechaEntregaProyectoCreado = LocalDate.of(anioProyectoCreado, mesProyectoCreado, diaProyectoCreado);
							unJefeProyecto.crearProyecto(codigoProyectoCreado, nombreProyectoCreado, jefeProyectoCreado, bandEstado, porcentajeProyectoCreado,fechaEntregaProyectoCreado);
							break;
							
						case 2:
							System.out.println("Selecciono Crear Tarea");
							
							int codigoTarea, codigoProyectoTarea, porcentajeTarea,diaEntregaTarea, mesEntregaTarea, anioEntregaTarea,auxProyectoAsignado=0;
							String observacionTarea, descripcionTarea;
							boolean estadoTarea=false, bandTarea;
							LocalDate fechaEntregaTarea;
							
							if(TablaProyectos.proyectos.isEmpty()==false) {
								bandTarea=false;
								do{
									System.out.println("Ingrese el Codigo de la Tarea");
									codigoTarea = scan.nextInt();
									if(TablaTareas.tareas.isEmpty()==true){
										System.out.println("Codigo Disponibles");
										bandTarea=true;
									}
									else {
										for(int i =0; i<TablaTareas.tareas.size();i++) {
											if(codigoTarea == TablaTareas.tareas.get(i).getCodigo()) {
												bandTarea=true;
											}
										}
										if(bandTarea==true){
											System.out.println("El codigo ya esta siendo usado");
											bandTarea=false;
										}
										else {
											System.out.println("Codigo Disponible");
											bandTarea=true;
										}
									}
								}while (bandTarea==false);
								
								bandTarea=false;
								do{
									System.out.println("Ingrese el Codigo del Proyecto al que pertenece la tarea");
									codigoProyectoTarea = scan.nextInt();
									for(int i=0; i<TablaProyectos.proyectos.size();i++) {
										if(codigoProyectoTarea==TablaProyectos.proyectos.get(i).getCodigo()) {
											bandTarea=true;
											auxProyectoAsignado=i;
										}
									}
									if(bandTarea==true){
										System.out.println("Codigo de Proyecto encontrado");
									}
									else {
										System.out.println("El codigo del Proyecto no ha sido encontrado");
									}
								}while(bandTarea==false);
								
								System.out.println("Ingrese una descripcion de la Tarea");
								descripcionTarea = scan.next();
								System.out.println("Ingrese la Observacion de la Tarea");
								observacionTarea = scan.next();
								
								Boolean validoTarea=false;
								do {
									System.out.println("Ingrese el estado de la tarea(Activo o Inactivo)");
									String estadoTareaCreada=scan.next();
									
									switch (estadoTareaCreada) {
									case "Activo":
										estadoTarea = true;
										validoTarea=true;
										break;
									case "Inactivo":
										estadoTarea= false;
										validoTarea=true;
										break;
									default:
										System.out.println("Estado no aceptable, respete mayusculas y minusculas");
										break;
									}
									}while(validoTarea==false);
								
								System.out.println("Ingrese el Porcentaje de la Tarea");
								porcentajeTarea = scan.nextInt();
								System.out.println("Ingrese el dia de Entrega");
								diaEntregaTarea = scan.nextInt();
								System.out.println("Ingrese el mes de Entrega");
								mesEntregaTarea = scan.nextInt();
								System.out.println("Ingrese el anio de Entrega");
								anioEntregaTarea = scan.nextInt();
								fechaEntregaTarea = LocalDate.of(anioEntregaTarea, mesEntregaTarea, diaEntregaTarea);
								unJefeProyecto.crearTarea(codigoTarea, auxProyectoAsignado, descripcionTarea, observacionTarea, estadoTarea, porcentajeTarea,fechaEntregaTarea);
							}
							else {
								System.out.println("No hay proyectos en la tabla Proyectos, cree un proyecto");
							}
							break;
							
						case 3:
							String usuarioAgregar, tipoUsuarioAgregar;
							int tareaAgregar, auxAgregar = 0, auxPasanteAgregar = 0, auxTareaProyectoAgregar = 0,
									auxDesarrolladorAgregar = 0;
							boolean bandAgregar = false;
							Pasante unPasanteAgregar = new Pasante();
							PersonalDeDesarrollo unDesarrolladorAgregar = new PersonalDeDesarrollo();

							System.out.println("Selecciono Agregar Usuarios a la tarea");

							if (TablaPasantes.pasantes.isEmpty() == true && TablaPasantes.pasantes.isEmpty() == true) {
								System.out.println("Las tablas Pasantes y Desarrolladores estan vacias");
							} else {
								if (TablaTareas.tareas.isEmpty() == true) {
									System.out.println("La tabla tareas esta vacia");
								} else {

									do {
										System.out.println("Ingrese el Codigo de la Tarea a agregar Usuarios");
										tareaAgregar = scan.nextInt();

										for (int i = 0; i < TablaTareas.tareas.size(); i++) {
											if (tareaAgregar == TablaTareas.tareas.get(i).getCodigo()) {
												System.out.println("Codigo de tarea encontrada");
												bandAgregar = true;
												auxAgregar = i;
											}
										}
										if (bandAgregar == false) {
											System.out.println("No se encontro el codigo de la tarea");
										}
									} while (bandAgregar == false);

									bandAgregar = false;
									do {
										System.out.println(
												"Ingrese el nombre del usuario a agregar (Debe se un Desarrollador o Pasante)");
										usuarioAgregar = scan.next();
										tipoUsuarioAgregar = tipoUsuario(usuarioAgregar);

										if (tipoUsuarioAgregar.equals("Pasante")) {
											bandAgregar = true;

											unPasanteAgregar = buscarPasante(usuarioAgregar);

											boolean bandOmegaAuxiliar = false;
											for (int i = 0; i < TablaPasantes.pasantes.size(); i++) {
												bandOmegaAuxiliar = TablaPasantes.pasantes.get(i).getUsername()
														.equals(usuarioAgregar);
												if (bandOmegaAuxiliar == true) {
													auxPasanteAgregar = i;
												}
											}

											TablaTareas.tareas.get(auxAgregar).getPasantes().add(unPasanteAgregar);
											TablaPasantes.pasantes.get(auxPasanteAgregar).setEstado(true);

											for (int i = 0; i < TablaProyectos.proyectos.size(); i++) {
												if (TablaTareas.tareas.get(auxAgregar).getNombreProyecto().equals(
														TablaProyectos.proyectos.get(i).getNombreProyecto()) == true) {

													System.out.println("Guardando Tarea en la Tabla Proyecto");

													for (int y = 0; y < TablaProyectos.proyectos.get(i).getTareas()
															.size(); y++) {
														if (TablaProyectos.proyectos.get(i).getTareas().get(y)
																.getCodigo() == tareaAgregar) {
															auxTareaProyectoAgregar = y;
														}
													}

													Tarea tareaAgregarProyecto = TablaTareas.tareas.get(auxAgregar);
													TablaProyectos.proyectos.get(i).getTareas()
															.set(auxTareaProyectoAgregar, tareaAgregarProyecto);
												}
											}
										} else if (tipoUsuarioAgregar.equals("Desarrollador")) {
											bandAgregar = true;

											unDesarrolladorAgregar = buscarDesarrollador(usuarioAgregar);

											boolean bandOmegaAuxiliar = false;
											for (int i = 0; i < TablaDesarrolladores.desarrolladores.size(); i++) {
												bandOmegaAuxiliar = TablaDesarrolladores.desarrolladores.get(i)
														.getUsername().equals(usuarioAgregar);
												if (bandOmegaAuxiliar == true) {
													auxDesarrolladorAgregar = i;
												}
											}

											TablaTareas.tareas.get(auxAgregar).getDesarrolladores()
													.add(unDesarrolladorAgregar);
											TablaDesarrolladores.desarrolladores.get(auxDesarrolladorAgregar)
													.setEstado(true);

											for (int i = 0; i < TablaProyectos.proyectos.size(); i++) {
												if (TablaTareas.tareas.get(auxAgregar).getNombreProyecto().equals(TablaProyectos.proyectos.get(i).getNombreProyecto()) == true) {

													System.out.println("Guardando Tarea en la Tabla Proyecto");

													for (int y = 0; y < TablaProyectos.proyectos.get(i).getTareas().size(); y++) {
														if (TablaProyectos.proyectos.get(i).getTareas().get(y).getCodigo() == tareaAgregar) {
															auxTareaProyectoAgregar = y;
														}
													}

													Tarea tareaAgregarProyecto = TablaTareas.tareas.get(auxAgregar);
													TablaProyectos.proyectos.get(i).getTareas().set(auxTareaProyectoAgregar, tareaAgregarProyecto);

												}
											}
										} else if (tipoUsuarioAgregar.equals(null)) {
											System.out.println("Usuario no Encontrado");
										} else {
											System.out.println("Tipo de usuario incorrecto");
										}
									} while (bandAgregar == false);
								}
							}
							break;
							case 4:
								System.out.println("Mostrando todas las tareas: ");
								unJefeProyecto.listarTareas();

								break;
							case 5:
								int codProyecto;
								if (TablaProyectos.proyectos.isEmpty() == true) {
									System.out.println("La tabla proyectos se encuentra vacia");
								} else {
									System.out.println("Ingrese el codigo del Proyecto a buscar");
									codProyecto = scan.nextInt();
									unJefeProyecto.buscarProyectoCod(codProyecto);
								}

								break;
							case 6:
								int codTarea;

								if (TablaTareas.tareas.isEmpty() == true) {
									System.out.println("La tabla tareas se encuentra vacia");
								} else {
									System.out.println("Ingrese el codigo de la Tarea a buscar");
									codTarea = scan.nextInt();
									unJefeProyecto.buscarTareaCod(codTarea);
								}

								break;
							case 7:
								int codTareaModificar, opcionTareaModificar;
								String obsTareaModificar;
								boolean estadoTareaModificar;

								if (TablaTareas.tareas.isEmpty() == true) {
									System.out.println("La tabla tareas se encuentra vacia");
								}

								else {

									System.out.println("1)Modificar Observacion");
									System.out.println("2)Modificar Estado");
									opcionTareaModificar = scan.nextInt();

									if (opcionTareaModificar == 1) {
										System.out.println("Ingrese el codigo de la tarea a modificar");
										codTareaModificar = scan.nextInt();
										System.out.println("Ingrese la observacion de la tarea");
										obsTareaModificar = scan.next();
										unJefeProyecto.modificarTareaObs(codTareaModificar, obsTareaModificar);
									} else if (opcionTareaModificar == 2) {
										System.out.println("Ingrese el codigo de la tarea a modificar");
										codTareaModificar = scan.nextInt();
										System.out.println("Ingrese el estado de la tarea (true o false)");
										estadoTareaModificar = scan.nextBoolean();
										unJefeProyecto.modificarTareaEstado(codTareaModificar, estadoTareaModificar);
									} else {
										System.out.println("Opcion Ivalida");
									}
								}
								break;
							/*
							 * 
							 * 
							 * 
							 */
							case 8:
							System.out.println("Saliendo del Programa");
							break;
						default:
							System.out.println("Opcion invalida");
							break;
						}
					} while (opcionJefe != 8);
					break;
				case "RRHHS":
					ResponsableDeRecursos unRRHH = buscarRRHHS(username);

					int opcionRRHH;
					do {
						System.out.println("----------Menu RRHHS---------");
						System.out.println("Escoja una opcion");
						System.out.println("1) Mostrar Pasantes");
						System.out.println("2) Mostrar Desarrolladores sin tarea");
						System.out.println("3) Dar de Alta a un Desarrollador");
						System.out.println("4) Salir");
						opcionRRHH = scan.nextInt();

						switch (opcionRRHH) {
						case 1:
							unRRHH.mostrarDatosPasantes();
							break;
						case 2:
							unRRHH.listarDesarrolladoresSinTarea();
							break;
						case 3:
							do {
								System.out.println("Ingrese el username del desarrollador a dar de baja");
								username = scan.next();
								if (buscarDesarrollador(username) == null && buscarPasante(username) == null) {
									System.out.println("Desarrollador no encontrado");
								}
							} while (buscarDesarrollador(username) == null && buscarPasante(username) == null);
							unRRHH.darAltaDesarrollador(username);
							break;
						case 4:
							System.out.println("Saliendo del Programa");
							break;
						default:
							System.out.println("Opcion invalida");
							break;
						}
					} while (opcionRRHH != 4);
					break;
				}

			}
				break;
			case "p":
			{
				if (first==true) {
				Presidente unPresidente= new Presidente();
				Pasante unPasante = new Pasante();
				PersonalDeDesarrollo unDesarrollador = new PersonalDeDesarrollo();
				ResponsableDeRecursos unRRHH = new ResponsableDeRecursos();
				JefeProyecto unJefeProyecto = new JefeProyecto();
				Proyecto unProyecto = new Proyecto();
				Tarea unaTarea = new Tarea();
				LocalDate fecha=LocalDate.now();
				String passPredef="1234",techPredef="Java";
				ArrayList <PersonalDeDesarrollo> desarrolladores = new ArrayList <PersonalDeDesarrollo>();
				ArrayList <Pasante> pasantes = new ArrayList <Pasante>();
				
				unPresidente.setDni(11111111);
				unPresidente.setfIngreso(fecha);
				unPresidente.setfNac(fecha);
				unPresidente.setLegajo(1);
				unPresidente.setNombre("Presidente");
				unPresidente.setPassword(passPredef);
				unPresidente.setRol("Gerente");
				unPresidente.setUsername("Presidente");
				
				unPasante.setDni(11111112);
				unPasante.setEstado(false);
				unPasante.setFechaInP(fecha);
				unPasante.setFechaFinP(fecha);
				unPasante.setfNac(fecha);
				unPasante.setNombre("Pasante");
				unPasante.setPassword(passPredef);
				unPasante.setRol("Programador");
				unPasante.setTecnologia(techPredef);
				unPasante.setUniversidad("Harvard");
				unPasante.setUsername("Pasante");
				
				unDesarrollador.setDni(11111113);
				unDesarrollador.setEstado(false);
				unDesarrollador.setfIngreso(fecha);
				unDesarrollador.setfNac(fecha);
				unDesarrollador.setLegajo(2);
				unDesarrollador.setNombre("Desarrollador");
				unDesarrollador.setNroContacto(1111111);
				unDesarrollador.setPassword(passPredef);
				unDesarrollador.setRol("Diseñador Grafico");
				unDesarrollador.setTecnologia(techPredef);
				unDesarrollador.setTituloPro("Licenciado");
				unDesarrollador.setUsername("Desarrollador");
				
				
				unRRHH.setDni(11111114);
				unRRHH.setfIngreso(fecha);
				unRRHH.setfNac(fecha);
				unRRHH.setLegajo(3);
				unRRHH.setNombre("RRHH");
				unRRHH.setPassword(passPredef);
				unRRHH.setRol("Gerente");
				unRRHH.setUsername("RRHH");
				
				unJefeProyecto.setDni(11111115);
				unJefeProyecto.setEstado(false);
				unJefeProyecto.setfIngreso(fecha);
				unJefeProyecto.setfNac(fecha);
				unJefeProyecto.setLegajo(4);
				unJefeProyecto.setNombre("Jefe Proyecto");
				unJefeProyecto.setNroContacto(1111112);
				unJefeProyecto.setPassword(passPredef);
				unJefeProyecto.setRol("Jefe de Proyecto");
				unJefeProyecto.setTecnologia(techPredef);
				unJefeProyecto.setTituloPro("Licenciado");
				unJefeProyecto.setUsername("JefeProyecto");
								
				unProyecto.setCodigo(1);
				unProyecto.setEstado(true);
				unProyecto.setFechaEntrega(fecha);
				unProyecto.setJefeProyecto(unJefeProyecto.getNombre());
				unProyecto.setNombreProyecto("Proyecto Predef");
				unProyecto.setPorcentaje(50);
				
				unaTarea.setCodigo(1);
				unaTarea.setDesarrolladores(desarrolladores);
				unaTarea.setEstado(false);
				unaTarea.setFechaEntrega(fecha);
				unaTarea.setNombreProyecto(unProyecto.getNombreProyecto());
				unaTarea.setObservacion("Ninguna");
				unaTarea.setPasantes(pasantes);
				unaTarea.setPorcentaje(30);
				unaTarea.setTareas("Tarea1");
				
				TablaTareas.tareas.add(unaTarea);
				unProyecto.getTareas().add(unaTarea);
				TablaProyectos.proyectos.add(unProyecto);
				
				TablaJefesProyectos.jefesProyectos.add(unJefeProyecto);
				TablaPasantes.pasantes.add(unPasante);
				TablaRRHH.RRHHs.add(unRRHH);
				TablaDesarrolladores.desarrolladores.add(unDesarrollador);
				TablaPresidentes.presidentes.add(unPresidente);
				first=false;
				System.out.println("Usuarios, proyectos y tareas cargados exitosamente");
				}
				else
					System.out.println("Ya ha ingresado usuarios previamente o ha ingresado usuarios de forma manual");
			}
			break;
			case "Off":
				System.out.println("Cerrando el programa, adios!");
				break;
			default:
				System.out.println("Opcion inexistente!");
				break;
			}
		} while (!respuesta.equals("Off"));
		scan.close();

	}

	private static boolean buscarUsuario(String username) {
		boolean band = false;

		for (int i = 0; i < TablaDesarrolladores.desarrolladores.size(); i++) {
			band = TablaDesarrolladores.desarrolladores.get(i).getUsername().equals(username);
			if (band == true)
				i = TablaDesarrolladores.desarrolladores.size();
		}

		if (band == true) {
			System.out.println("Ya existe ese username");
		} else {
			for (int i = 0; i < TablaPasantes.pasantes.size(); i++) {
				band = TablaPasantes.pasantes.get(i).getUsername().equals(username);
				if (band == true)
					i = TablaPasantes.pasantes.size();
			}
			if (band == true) {
				System.out.println("Ya existe ese username");
			} else {
				for (int i = 0; i < TablaRRHH.RRHHs.size(); i++) {
					band = TablaRRHH.RRHHs.get(i).getUsername().equals(username);
					if (band == true)
						i = TablaRRHH.RRHHs.size();
				}
				if (band == true) {
					System.out.println("Ya existe ese username");
				} else {
					for (int i = 0; i < TablaPresidentes.presidentes.size(); i++) {
						band = TablaPresidentes.presidentes.get(i).getUsername().equals(username);
						if (band == true)
							i = TablaPresidentes.presidentes.size();
					}
					if (band == true) {
						System.out.println("Ya existe ese username");
					} else {
						for (int i = 0; i < TablaJefesProyectos.jefesProyectos.size(); i++) {
							band = TablaJefesProyectos.jefesProyectos.get(i).getUsername().equals(username);
							if (band == true)
								i = TablaJefesProyectos.jefesProyectos.size();
						}
						if (band == true) {
							System.out.println("Ya existe ese username");
						} else
							System.out.println("Username disponible!");
					}
				}
			}
		}
		return band;
	}

	private static boolean buscarUsuarioInicio(String username, String password) {
		boolean band = false;

		for (int i = 0; i < TablaDesarrolladores.desarrolladores.size(); i++) {
			band = TablaDesarrolladores.desarrolladores.get(i).getUsername().equals(username);
			if (band == true) {
				band = TablaDesarrolladores.desarrolladores.get(i).getPassword().equals(password);
				return band;
			}
		}
		for (int i = 0; i < TablaPasantes.pasantes.size(); i++) {
			band = TablaPasantes.pasantes.get(i).getUsername().equals(username);
			if (band == true) {
				band = TablaPasantes.pasantes.get(i).getPassword().equals(password);
				return band;
			}
		}
		for (int i = 0; i < TablaRRHH.RRHHs.size(); i++) {
			band = TablaRRHH.RRHHs.get(i).getUsername().equals(username);
			if (band == true) {
				band = TablaRRHH.RRHHs.get(i).getPassword().equals(password);
				return band;
			}
		}

		for (int i = 0; i < TablaPresidentes.presidentes.size(); i++) {
			band = TablaPresidentes.presidentes.get(i).getUsername().equals(username);
			if (band == true) {
				band = TablaPresidentes.presidentes.get(i).getPassword().equals(password);
				return band;

			}
		}

		for (int i = 0; i < TablaJefesProyectos.jefesProyectos.size(); i++) {
			band = TablaJefesProyectos.jefesProyectos.get(i).getUsername().equals(username);
			if (band == true) {
				band = TablaJefesProyectos.jefesProyectos.get(i).getPassword().equals(password);
				return band;
			}
		}
		return band;
	}

	private static String tipoUsuario(String username) {
		boolean band = false;
		String tipo = null;

		for (int i = 0; i < TablaDesarrolladores.desarrolladores.size(); i++) {
			band = TablaDesarrolladores.desarrolladores.get(i).getUsername().equals(username);
			if (band == true) {
				tipo = "Desarrollador";
				return tipo;
			}
		}

		if (band == false) {
			for (int i = 0; i < TablaPasantes.pasantes.size(); i++) {
				band = TablaPasantes.pasantes.get(i).getUsername().equals(username);
				if (band == true) {
					tipo = "Pasante";
					return tipo;
				}
			}
			if (band == false) {
				for (int i = 0; i < TablaRRHH.RRHHs.size(); i++) {
					band = TablaRRHH.RRHHs.get(i).getUsername().equals(username);
					if (band == true) {
						tipo = "RRHHS";
						return tipo;
					}
				}
				if (band == false) {
					for (int i = 0; i < TablaPresidentes.presidentes.size(); i++) {
						band = TablaPresidentes.presidentes.get(i).getUsername().equals(username);
						if (band == true) {
							tipo = "Presidente";
							return tipo;
						}
					}
					if (band == false) {
						for (int i = 0; i < TablaJefesProyectos.jefesProyectos.size(); i++) {
							band = TablaJefesProyectos.jefesProyectos.get(i).getUsername().equals(username);
							if (band == true) {
								tipo = "JefeP";
								return tipo;
							}
						}

					}
				}
			}
		}
		return tipo;
	}

	private static PersonalDeDesarrollo buscarDesarrollador(String username) {
		boolean band = false;

		for (int i = 0; i < TablaDesarrolladores.desarrolladores.size(); i++) {
			band = TablaDesarrolladores.desarrolladores.get(i).getUsername().equals(username);
			if (band == true) {
				return TablaDesarrolladores.desarrolladores.get(i);
			}
		}
		return null;
	}

	private static JefeProyecto buscarJefeProyecto(String username) {
		boolean band = false;
		for (int i = 0; i < TablaJefesProyectos.jefesProyectos.size(); i++) {
			band = TablaJefesProyectos.jefesProyectos.get(i).getUsername().equals(username);
			if (band == true) {
				return TablaJefesProyectos.jefesProyectos.get(i);
			}
		}
		return null;
	}

	private static Presidente buscarPresidente(String username) {
		boolean band = false;
		for (int i = 0; i < TablaPresidentes.presidentes.size(); i++) {
			band = TablaPresidentes.presidentes.get(i).getUsername().equals(username);
			if (band == true) {
				return TablaPresidentes.presidentes.get(i);
			}
		}
		return null;
	}

	private static ResponsableDeRecursos buscarRRHHS(String username) {
		boolean band = false;
		for (int i = 0; i < TablaRRHH.RRHHs.size(); i++) {
			band = TablaRRHH.RRHHs.get(i).getUsername().equals(username);
			if (band == true) {
				return TablaRRHH.RRHHs.get(i);
			}
		}
		return null;
	}

	private static Pasante buscarPasante(String username) {
		boolean band = false;
		for (int i = 0; i < TablaPasantes.pasantes.size(); i++) {
			band = TablaPasantes.pasantes.get(i).getUsername().equals(username);
			if (band == true) {
				return TablaPasantes.pasantes.get(i);
			}
		}
		return null;
	}

}