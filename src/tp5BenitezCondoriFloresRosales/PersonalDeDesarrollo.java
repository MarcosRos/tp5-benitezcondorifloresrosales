package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;

import tp5BenitezCondoriFloresRosales.tablas.TablaProyectos;
import tp5BenitezCondoriFloresRosales.tablas.TablaTareas;

public class PersonalDeDesarrollo extends PersonalContratado {

	private int nroContacto;
	private String tituloPro;
	private String tecnologia;
	private Boolean estado;

	public PersonalDeDesarrollo() {
		// TODO Auto-generated constructor stub
	}

	public PersonalDeDesarrollo(int dni, String nombre, LocalDate fNac, String rol, String username, String password,
			LocalDate fIngreso, int legajo,int nroContacto, String tituloPro, String tecnologia, Boolean estado) {
		super(dni, nombre, fNac, rol, username, password, fIngreso, legajo);
		this.nroContacto = nroContacto;
		this.tituloPro = tituloPro;
		this.tecnologia = tecnologia;
		this.estado = estado;
		// TODO Auto-generated constructor stub
	}

    public void listarTareas() {
		
		if(TablaTareas.tareas.isEmpty()==true){
			System.out.println("La tabla tareas se encuentra vacias");
		}
		else{
			TablaTareas.tareas.stream().forEach(System.out::println);
		}
	}

    public void buscarProyectoCod(int cod) {
		int cont = 0;
		boolean ban = false;
		while (cont < TablaProyectos.proyectos.size()) {
			
			if (cod == TablaProyectos.proyectos.get(cont).getCodigo()) {
				System.out.println("El proyecto encontrado es: "+ TablaProyectos.proyectos.get(cont));
				ban = true;
			}
			cont++;
		}
		
		if (ban == false) {
			System.out.println("No se encontro el codigo del proyecto.");
		}
		
	}

	public void buscarTareaCod(int cod) {
		int cont = 0;
		boolean ban = false;
		while (cont < TablaTareas.tareas.size()) {
			
			if (cod == TablaTareas.tareas.get(cont).getCodigo()) {
				System.out.println("La tarea encontrada es: "+ TablaTareas.tareas.get(cont));
				ban = true;
			}
			cont++;
		}
		
		if (ban == false) {
			System.out.println("No se encontro el codigo de la tarea.");
		}
	}

	public void modificarTareaObs(int cod, String obs) {
		int cont = 0;
		boolean ban=false;
		while (cont < TablaTareas.tareas.size()) {
			
			if (cod == TablaTareas.tareas.get(cont).getCodigo()) {
				TablaTareas.tareas.get(cont).setObservacion(obs);
				ban=true;
			}
				cont++;
			
		}
		if (ban ==false) {
			System.out.println("El codigo de la tarea no se encontro");
		}
		else {
			System.out.println("Observacion de la Tarea modificado");
		}
	}
	
	public void modificarTareaEstado(int cod, boolean estado){
		int cont = 0;
		boolean ban=false;
		while (cont != TablaTareas.tareas.size()) {
			
			if (cod == TablaTareas.tareas.get(cont).getCodigo()) {
				TablaTareas.tareas.get(cont).setEstado(estado);
				ban=true;
			}
				cont++;
		}
		if (ban ==false) {
			System.out.println("El codigo de la tarea no se encontro");
		}
		else {
			System.out.println("Estado de la Tarea modificado");
		}
	}

    
	public int getNroContacto() {
		return nroContacto;
	}

	public void setNroContacto(int nroContacto) {
		this.nroContacto = nroContacto;
	}

	public String getTituloPro() {
		return tituloPro;
	}

	public void setTituloPro(String tituloPro) {
		this.tituloPro = tituloPro;
	}

	public String getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		String ocupado;
		if (estado==true) {
			ocupado="Ocupado";
		}
		else
		{
			ocupado="Libre";
		}
		return "PersonalDeDesarrollo [" + super.toString() +", nroContacto=" + nroContacto + ", tituloPro=" + tituloPro + ", tecnologia="
				+ tecnologia + ", estado=" + ocupado + "]";
	}

	
	
	}

