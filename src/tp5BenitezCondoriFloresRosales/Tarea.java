package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;
import java.util.ArrayList;

public class Tarea {
    private int codigo;
    private String nombreProyecto;
    private String tareas;
    private String observacion;
    private ArrayList<Pasante> pasantes = new ArrayList<Pasante>();
    private ArrayList<PersonalDeDesarrollo> desarrolladores = new ArrayList<PersonalDeDesarrollo>();
    private Boolean estado;
    private int porcentaje;
    private LocalDate FechaEntrega;
    
    public Tarea() {
        // TODO Auto-generated constructor stub
    }

    public Tarea(int codigo, String nombreProyecto, String tareas, String observacion, ArrayList<Pasante> pasantes,
            ArrayList<PersonalDeDesarrollo> desarrolladores, Boolean estado, int porcentaje, LocalDate fechaEntrega) {
        super();
        this.codigo = codigo;
        this.nombreProyecto = nombreProyecto;
        this.tareas = tareas;
        this.observacion = observacion;
        this.pasantes = pasantes;
        this.desarrolladores = desarrolladores;
        this.estado = estado;
        this.porcentaje = porcentaje;
        this.FechaEntrega = fechaEntrega;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getTareas() {
        return tareas;
    }

    public void setTareas(String tareas) {
        this.tareas = tareas;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public ArrayList<Pasante> getPasantes() {
        return pasantes;
    }

    public void setPasantes(ArrayList<Pasante> pasantes) {
        this.pasantes = pasantes;
    }

    public ArrayList<PersonalDeDesarrollo> getDesarrolladores() {
        return desarrolladores;
    }

    public void setDesarrolladores(ArrayList<PersonalDeDesarrollo> desarrolladores) {
        this.desarrolladores = desarrolladores;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }

    public LocalDate getFechaEntrega() {
        return FechaEntrega;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        FechaEntrega = fechaEntrega;
    }

    @Override
    public String toString() {
    	String boolAString;
    	if(estado==false){
    		boolAString="Inactivo";
    	}
    	else {
    		boolAString="Activo";
    	}
        return "Tarea [codigo=" + codigo + ", nombreProyecto=" + nombreProyecto + ", tareas=" + tareas
                + ", observacion=" + observacion + ", pasantes=" + pasantes + ", desarrolladores=" + desarrolladores
                + ", estado=" + boolAString + ", porcentaje=" + porcentaje + ", FechaEntrega=" + FechaEntrega + "]";
    }
    
}