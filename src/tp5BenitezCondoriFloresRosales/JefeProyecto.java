package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;
import java.util.ArrayList;

import tp5BenitezCondoriFloresRosales.tablas.TablaProyectos;
import tp5BenitezCondoriFloresRosales.tablas.TablaTareas;

public class JefeProyecto extends PersonalDeDesarrollo {

    public JefeProyecto() {
        // TODO Auto-generated constructor stub
    }

    public JefeProyecto(int dni, String nombre, LocalDate fNac, String rol, String username, String password,
            LocalDate fIngreso, int legajo, int nroContacto, String tituloPro, String tecnologia, Boolean estado) {
        super(dni, nombre, fNac, rol, username, password, fIngreso, legajo, nroContacto, tituloPro, tecnologia,estado);
        // TODO Auto-generated constructor stub
    }

    public void crearProyecto(int codP, String nombreP, String jefeP, boolean estadoP, int porcentajeP, LocalDate fechaEntregaP) {
        ArrayList<Tarea> tareasP = new ArrayList<Tarea>();
        Proyecto proyecto = new Proyecto(codP, nombreP, tareasP, jefeP, estadoP, porcentajeP, fechaEntregaP);
        TablaProyectos.proyectos.add(proyecto);
        System.out.println("Proyecto agregado a la lista de Proyectos");
    }

    public void crearTarea(int codigoT, int auxProyecto, String descT, String observacionT, boolean estadoT, int porcentajeT, LocalDate fechaEntregaT) {
        String nombreProyectoT;
        ArrayList<Pasante> pasantesT = new ArrayList<Pasante>();
        ArrayList<PersonalDeDesarrollo> desarrolladoresT = new ArrayList<PersonalDeDesarrollo>();

        nombreProyectoT=TablaProyectos.proyectos.get(auxProyecto).getNombreProyecto();

        Tarea tareaCreada = new Tarea(codigoT, nombreProyectoT, descT, observacionT, pasantesT, desarrolladoresT, estadoT, porcentajeT, fechaEntregaT);
        TablaTareas.tareas.add(tareaCreada);
        TablaProyectos.proyectos.get(auxProyecto).getTareas().add(tareaCreada);
    }

}

