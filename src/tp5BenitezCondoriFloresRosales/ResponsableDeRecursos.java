package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;

import tp5BenitezCondoriFloresRosales.tablas.TablaDesarrolladores;
import tp5BenitezCondoriFloresRosales.tablas.TablaPasantes;


public class ResponsableDeRecursos extends PersonalContratado {

	public ResponsableDeRecursos() {
		// TODO Auto-generated constructor stub
	}
	
	public ResponsableDeRecursos(int dni, String nombre, LocalDate fNac, String rol, String username, String password,
			LocalDate fIngreso, int legajo) {
		super(dni, nombre, fNac, rol, username, password, fIngreso, legajo);
		// TODO Auto-generated constructor stub
	}

	public void mostrarDatosPasantes() {
		if (TablaPasantes.pasantes.isEmpty()) {
			System.out.println("La tabla de pasantes esta vacia");
		}
		else
			TablaPasantes.pasantes.stream().forEach(System.out::println);
	}

	
	public void listarDesarrolladoresSinTarea() {
		TablaPasantes.pasantes.stream().filter(a -> a.getEstado()==false).forEach(System.out::println);
		TablaDesarrolladores.desarrolladores.stream().filter(a -> a.getEstado()==false).forEach(System.out::println);	
		}
	
	
	public void darAltaDesarrollador(String username) {
		Boolean band=false;
		for (int i = 0; i < TablaPasantes.pasantes.size(); i++) {
			band=TablaPasantes.pasantes.get(i).getUsername().equals(username);
			if (band==true)
			{
				TablaPasantes.pasantes.remove(i);
			}
		}
		for (int i = 0; i < TablaDesarrolladores.desarrolladores.size(); i++) {
			band = TablaDesarrolladores.desarrolladores.get(i).getUsername().equals(username);
			if (band == true)
			{
				TablaDesarrolladores.desarrolladores.remove(i);
			}
		}
	}
}
