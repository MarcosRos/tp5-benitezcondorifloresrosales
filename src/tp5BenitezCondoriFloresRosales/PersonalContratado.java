package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;

public abstract class PersonalContratado extends Empleado {
	public static final double BONO_FUNCION= 20000;
	private LocalDate fIngreso;
	private int legajo;

	public PersonalContratado() {
		// TODO Auto-generated constructor stub
	}
	
	public PersonalContratado(int dni, String nombre, LocalDate fNac, String rol, String username, String password, LocalDate fIngreso, int legajo) {
		super(dni, nombre, fNac, rol, username, password);
		this.fIngreso = fIngreso;
		this.legajo = legajo;
		// TODO Auto-generated constructor stub
	}

	public int calcularAntiguedad() {
		int anios;
		anios = LocalDate.now().getYear() - fIngreso.getYear();
		if (LocalDate.now().getMonthValue() < fIngreso.getMonthValue()
				|| (LocalDate.now().getMonthValue() == fIngreso.getMonthValue()
						&& LocalDate.now().getDayOfMonth() < fIngreso.getDayOfMonth()))
			anios--;
		return anios;
	}

	public LocalDate getfIngreso() {
		return fIngreso;
	}

	public void setfIngreso(LocalDate fIngreso) {
		this.fIngreso = fIngreso;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	@Override
	public double calcularSueldo() {
		// TODO Auto-generated method stub
		int anios=calcularAntiguedad();
		double sueldoFinal;
		if (anios>5)
		{
			sueldoFinal=SUELDO_BASICO+BONO_FUNCION+BONO_FUNCION*0.40;
		}
		else
		{
			sueldoFinal=SUELDO_BASICO+BONO_FUNCION;
		}
		return sueldoFinal;
	}

	@Override
	public String toString() {
		return "PersonalContratado ["+ super.toString()+"fIngreso=" + fIngreso + ", legajo=" + legajo + "]";
	}
	
	
}
