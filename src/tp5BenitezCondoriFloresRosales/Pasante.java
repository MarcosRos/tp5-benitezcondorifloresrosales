package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;


import tp5BenitezCondoriFloresRosales.tablas.TablaTareas;
import tp5BenitezCondoriFloresRosales.tablas.TablaProyectos;

public class Pasante extends Empleado {
	
	private String universidad;
	private LocalDate fechaInP;
	private LocalDate fechaFinP;
	private String tecnologia;
	private Boolean estado;
	
	public Pasante() {
		// TODO Auto-generated constructor stub
	}
	
	public Pasante(int dni, String nombre, LocalDate fNac, String rol, String username, String password, String universidad, LocalDate fechaInP, LocalDate fechaFinP, String tecnologia, Boolean estado) {
		super(dni, nombre, fNac, rol, username, password);
		// TODO Auto-generated constructor stub
		this.universidad = universidad;
		this.fechaInP = fechaInP;
		this.fechaFinP = fechaFinP;
		this.tecnologia = tecnologia;
		this.estado = estado;
	}

    public void listarTareas() {
		
		if(TablaTareas.tareas.isEmpty()==true){
			System.out.println("La tabla tareas se encuentra vacias");
		}
		else{
			TablaTareas.tareas.stream().forEach(System.out::println);
		}
	}
	
	public void buscarProyectoCod(int cod) {
		int cont = 0;
		boolean ban = false;
		while (cont < TablaProyectos.proyectos.size()) {
			
			if (cod == TablaProyectos.proyectos.get(cont).getCodigo()) {
				System.out.println("El proyecto encontrado es: "+ TablaProyectos.proyectos.get(cont));
				ban = true;
			}
			cont++;
		}
		
		if (ban == false) {
			System.out.println("No se encontro el codigo del proyecto.");
		}
		
	}

	@Override
	public String toString() {
		String ocupado;
		if (estado==true) {
			ocupado="Ocupado";
		}
		else
		{
			ocupado="Libre";
		}
		return "Pasante ["+super.toString()+", universidad=" + universidad + ", fechaInP=" + fechaInP + ", fechaFinP=" + fechaFinP
				+ ", tecnologia=" + tecnologia + ", estado=" + ocupado + "]";
	}

	public void buscarTareaCod(int cod) {
		int cont = 0;
		boolean ban = false;
		while (cont < TablaTareas.tareas.size()) {
			
			if (cod == TablaTareas.tareas.get(cont).getCodigo()) {
				System.out.println("La tarea encontrada es: "+ TablaTareas.tareas.get(cont));
				ban = true;
			}
			cont++;
		}
		
		if (ban == false) {
			System.out.println("No se encontro el codigo de la tarea.");
		}
	}

	public void modificarTareaObs(int cod, String obs) {
		int cont = 0;
		boolean ban=false;
		while (cont < TablaTareas.tareas.size()) {
			
			if (cod == TablaTareas.tareas.get(cont).getCodigo()) {
				TablaTareas.tareas.get(cont).setObservacion(obs);
				ban=true;
			}
				cont++;
			
		}
		if (ban ==false) {
			System.out.println("El codigo de la tarea no se encontro");
		}
		else {
			System.out.println("Observacion de la Tarea modificado");
		}
	}
	
	public void modificarTareaEstado(int cod, boolean estado){
		int cont = 0;
		boolean ban=false;
		while (cont != TablaTareas.tareas.size()) {
			
			if (cod == TablaTareas.tareas.get(cont).getCodigo()) {
				TablaTareas.tareas.get(cont).setEstado(estado);
				ban=true;
			}
				cont++;
		}
		if (ban ==false) {
			System.out.println("El codigo de la tarea no se encontro");
		}
		else {
			System.out.println("Estado de la Tarea modificado");
		}
	}


	public String getUniversidad() {
		return universidad;
	}

	public void setUniversidad(String universidad) {
		this.universidad = universidad;
	}

	public LocalDate getFechaInP() {
		return fechaInP;
	}

	public void setFechaInP(LocalDate fechaInP) {
		this.fechaInP = fechaInP;
	}

	public LocalDate getFechaFinP() {
		return fechaFinP;
	}

	public void setFechaFinP(LocalDate fechaFinP) {
		this.fechaFinP = fechaFinP;
	}

	public String getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}
	
	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	@Override
	public double calcularSueldo() {
		// TODO Auto-generated method stub
		return SUELDO_BASICO;
	}
	
}