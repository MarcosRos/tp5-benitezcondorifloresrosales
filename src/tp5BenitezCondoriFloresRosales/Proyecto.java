package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;
import java.util.ArrayList;

public class Proyecto {
    private int codigo;
    private String nombreProyecto;
    private ArrayList<Tarea> tareas = new ArrayList<Tarea>();
    private String jefeProyecto;
    private Boolean estado;
    private int porcentaje;
    private LocalDate fechaEntrega;
    
    public Proyecto() {
        // TODO Auto-generated constructor stub
    }

    public Proyecto(int codigo, String nombreProyecto, ArrayList<Tarea> tareas, String jefeProyecto, Boolean estado,
            int porcentaje, LocalDate fechaEntrega) {
        super();
        this.codigo = codigo;
        this.nombreProyecto = nombreProyecto;
        this.tareas = tareas;
        this.jefeProyecto = jefeProyecto;
        this.estado = estado;
        this.porcentaje = porcentaje;
        this.fechaEntrega = fechaEntrega;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public ArrayList<Tarea> getTareas() {
        return tareas;
    }

    public void setTareas(ArrayList<Tarea> tareas) {
        this.tareas = tareas;
    }

    public String getJefeProyecto() {
        return jefeProyecto;
    }

    public void setJefeProyecto(String jefeProyecto) {
        this.jefeProyecto = jefeProyecto;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    @Override
    public String toString() {
    	String estadoProyecto;
		if (estado==true) {
			estadoProyecto="Activo";
		}
		else
		{
			estadoProyecto="Inactivo";
		}
        return "Proyecto [codigo=" + codigo + ", nombreProyecto=" + nombreProyecto + ", tareas=" + tareas
                + ", jefeProyecto=" + jefeProyecto + ", estado=" + estadoProyecto + ", porcentaje=" + porcentaje
                + ", fechaEntrega=" + fechaEntrega + "]";
    }
    
}
