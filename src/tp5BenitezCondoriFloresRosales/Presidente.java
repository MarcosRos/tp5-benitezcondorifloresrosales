package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;
import java.util.Comparator;

import tp5BenitezCondoriFloresRosales.tablas.TablaProyectos;

public class Presidente extends PersonalContratado {

	public Presidente() {
		// TODO Auto-generated constructor stub
	}
	
	public Presidente(int dni, String nombre, LocalDate fNac, String rol, String username, String password,
			LocalDate fIngreso, int legajo) {
		super(dni, nombre, fNac, rol, username, password, fIngreso, legajo);
		// TODO Auto-generated constructor stub
	}

	public void listarAvance() {
		TablaProyectos.proyectos.stream().sorted(Comparator.comparing(Proyecto::getPorcentaje)).forEach(System.out::println);
		
	}

	public void listarEstado() {
          TablaProyectos.proyectos.stream().sorted(Comparator.comparing(Proyecto::getEstado)).forEach(System.out::println);
            
        }

	class Sortbyporcentaje implements Comparator<Proyecto> {
	    // Used for sorting in ascending order of
	    // roll number
	    public int compare(Proyecto a, Proyecto b)
	    {
	        return a.getPorcentaje() - b.getPorcentaje();
	    }
	}
}
