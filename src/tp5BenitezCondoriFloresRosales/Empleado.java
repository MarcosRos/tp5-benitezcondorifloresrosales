package tp5BenitezCondoriFloresRosales;

import java.time.LocalDate;

public abstract class Empleado {
	public static final double SUELDO_BASICO = 50000;
	private int dni;
	private String nombre;
	private LocalDate fNac;
	private String rol;
	private String username;
	private String password;

	public Empleado() {
		// TODO Auto-generated constructor stub
	}
	
	public Empleado(int dni, String nombre, LocalDate fNac, String rol, String username, String password) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.fNac = fNac;
		this.rol = rol;
		this.username = username;
		this.password = password;
	}

	public void trabajar() {
		System.out.println("Estoy trabajando");
	}

	public abstract double calcularSueldo();

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getfNac() {
		return fNac;
	}

	public void setfNac(LocalDate fNac) {
		this.fNac = fNac;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static double getSueldoBasico() {
		return SUELDO_BASICO;
	}

	@Override
	public String toString() {
		return "dni=" + dni + ", nombre=" + nombre + ", fNac=" + fNac + ", rol=" + rol + ", username="
				+ username + "]";
	}
	

	
}
